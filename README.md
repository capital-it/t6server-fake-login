# README #

Quick overview of the project 

### What is this repository for? ###

Simulated Login-Logout Process:
In order to make Queue Allocation work efficiently it is important to use a simulated login/logout event to TPLUS. The service has been implemented and once testing is over today, it will be running on live environment. The service will perform login/logout for every state that has been configured. At the moment this functionality is be active only for WA. Adding another state is easy as it is controlled through a config file.

### How do I get set up? ###

Standard Windows Service running on server App01 - check WiKi for specific details

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* IT development team