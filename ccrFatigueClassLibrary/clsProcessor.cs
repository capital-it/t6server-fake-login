﻿
using ccrJobStatusUpdater.Classes.db.utils;
using iLogixImage.Classes;
using ILogixClassLibrary.Classes;

namespace ILogixClassLibrary
{
    public class clsProcessor
    {
        public void StartProcessing(Quartz.IJobExecutionContext context, ConfigStructures cfg)
        {
       //     SendMail.SendBasicEmail("Running StartProcessing", "ITOnly", cfg);
           
            ProcessLogEntries ple = new ProcessLogEntries();
            ple.Create(context, cfg);

         //   SendMail.SendBasicEmail("Completed Running Processing", "ITOnly", cfg);
        }
        
        public void StopProcessing()
        {
            
        }
    }
}