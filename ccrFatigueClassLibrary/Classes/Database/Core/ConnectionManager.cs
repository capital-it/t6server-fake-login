﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace ccrJobStatusUpdater.Classes 
{
    [System.Obsolete("Not in use. Connections are managed per class basis")]
    public class ConnectionManager
    {

        private static SqlConnection sqlConnection;

        /// <summary>
        /// Private constructor to adhere to Singleton pattern
        /// </summary>
        private ConnectionManager()
        {
        }

        /// <summary>
        /// Returns a connection object based on the type of connection (SQL, Oracle) requested
        /// and for its associated connection string
        /// </summary>
        /// <param name="type"></param>
        /// <param name="dbConnectionString"></param>
        /// <returns></returns>
        public static DbConnection getDbInstance(String dbConnectionString)
        {
            DbConnection dbConnection = null;
            try
            {
                        if (sqlConnection != null)
                            dbConnection = sqlConnection;
                        else
                        {
                            sqlConnection = new SqlConnection(dbConnectionString);
                            dbConnection = sqlConnection;
                        }
            }
            catch (Exception ex)
            {
                throw ex;
            }
             
            return dbConnection;
        }
    }
}