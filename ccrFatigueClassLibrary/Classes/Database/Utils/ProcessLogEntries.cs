﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Collections;
using MySql.Data.MySqlClient;
using ccrJobStatusUpdater.Classes.db.utils;
using ccrJobStatusUpdater.Classes;
using System.Diagnostics;
using System.Data;
using System.Threading;
using System.Web.UI;
using iLogixImage.Classes;

//using fatigueSettings = iCCRFatigueClassLibrary.Properties.Settings;

namespace ILogixClassLibrary.Classes
{
    public class ProcessLogEntries
    {
        //  private readonly dbServiceLayer _msg = Singleton<dbServiceLayer>.UniqueInstance;
        private DateTime _lastRun = DateTime.Now;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public ProcessLogEntries()
        {
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
        }

        /// <summary>
        /// Called by the <see cref="T:Quartz.IScheduler" /> when a <see cref="T:Quartz.ITrigger" />
        /// fires that is associated with the <see cref="T:Quartz.IJob" />.
        /// </summary>
        /// <param name="context">The execution context.</param>
        /// <param name="cfg">Used to send in configuration settings so the class does not have to worry</param>
        /// <remarks>
        /// The implementation may wish to set a  result object on the
        /// JobExecutionContext before this method exits.  The result itself
        /// is meaningless to Quartz, but may be informative to
        /// <see cref="T:Quartz.IJobListener" />s or
        /// <see cref="T:Quartz.ITriggerListener" />s that are watching the job's
        /// execution.
        /// </remarks>
        public void Create(Quartz.IJobExecutionContext context, ConfigStructures cfg)
        {

       //     SendMail.SendBasicEmail("Startup in Quartz", "Contact IT Team", cfg);

            DaylightSavings itm;

            #region setup all the states which can be enabled

            var rangeTimeZonesList = new List<DaylightSavings>();
            // 2 checks here, first is the state active and second, is the trigger activated
            // for the state itself? 
            if (cfg.ActivateVIC) // & context.Trigger.Key.ToString().Contains("VIC"))
            {

                itm.DriverId = cfg.DriverIdMelbourne;
                itm.StateCode = cfg.StateCodeMelbourne;
                itm.StateZone = cfg.StateZoneMelbourne;
                rangeTimeZonesList.Add(itm);
            }
            if (cfg.ActivateNSW) // & context.Trigger.Key.ToString().Contains("NSW"))
            {
                itm.DriverId = cfg.DriverIdSydney;
                itm.StateCode = cfg.StateCodeSydney;
                itm.StateZone = cfg.StateZoneSydney;
                rangeTimeZonesList.Add(itm);
            }
            if (cfg.ActivateQLD) // & context.Trigger.Key.ToString().Contains("QLD"))
            {
                itm.DriverId = cfg.DriverIdBrisbane;
                itm.StateCode = cfg.StateCodeBrisbane;
                itm.StateZone = cfg.StateZoneBrisbane;
                rangeTimeZonesList.Add(itm);
            }
            if (cfg.ActivateSA) // & context.Trigger.Key.ToString().Contains("SA"))
            {
                itm.DriverId = cfg.DriverIdAdelaide;
                itm.StateCode = cfg.StateCodeAdelaide;
                itm.StateZone = cfg.StateZoneAdelaide;
                rangeTimeZonesList.Add(itm);
            }
            if (cfg.ActivateWA) //& context.Trigger.Key.ToString().Contains("WA"))
            {
                itm.DriverId = cfg.DriverIdPerth;
                itm.StateCode = cfg.StateCodePerth;
                itm.StateZone = cfg.StateZonePerth;
                rangeTimeZonesList.Add(itm);
            }


            #endregion

        //    SendMail.SendBasicEmail("Range Setup is done", "Contact IT Team", cfg);

            try
            {
                using (var mySql = new MySqlConnection(cfg.MySQLConnection))
                {
                    mySql.Open();
                    // *****************
                    // steps to follow
                    // ***************** 
                    // find out the images folders for each state

                    var sql = string.Empty;


                    foreach (var state in rangeTimeZonesList)
                    {
                        sql = "SELECT COUNT(*) FROM cme WHERE " +
                              " MobileId = " + state.DriverId +
                              " AND Channel = " + state.StateCode;

                      //  SendMail.SendBasicEmail(string.Format("About to Execute {0}", sql), "Contact IT Team", cfg);

                        using (MySqlCommand command = new MySqlCommand(sql, mySql))
                        {
                            var cnt = command.ExecuteScalar();
                            if (Convert.ToInt16(cnt) == 0)
                            {
                                // first reset the zone for the driver being used, this is needed as only one distinct
                                // driver is setup in the database so if we don't change zone it will update wrong CME record
                                sql = "UPDATE CMO SET ZoneNow = '" + state.StateZone + "' WHERE MobileId = '" +
                                      state.DriverId + "'";
                               // SendMail.SendBasicEmail(string.Format("About to Execute {0}", sql), "Contact IT Team",cfg);

                                using (MySqlCommand command22 = new MySqlCommand(sql, mySql))
                                {
                                    command22.ExecuteNonQuery();
                                }
                                // run a login or logout
                                sql = GenerateFakeLogoutSql(state.StateCode, state.DriverId);
                              //  SendMail.SendBasicEmail(string.Format("About to Execute {0}", sql), "Contact IT Team",cfg);
                                using (MySqlCommand command2 = new MySqlCommand(sql, mySql))
                                {
                                    command2.ExecuteNonQuery();
                                }
                                Thread.Sleep(3000);
                                // run a login or logout
                                sql = GenerateFakeLoginSql(state.StateCode, state.DriverId);
//SendMail.SendBasicEmail(string.Format("About to Execute {0}", sql), "Contact IT Team",cfg);
                                using (MySqlCommand command3 = new MySqlCommand(sql, mySql))
                                {
                                    command3.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    mySql.Close();
                }
            }
            catch (Exception ex)
            {
                SendMail.SendBasicEmail(string.Format("Threw and Error {0}", ex.Message), "Contact IT Team",
                               cfg);
            }
        }

        public static string GenerateFakeLoginSql(string stateId, string mobileId)
        {
            var chikDate = DateTime.Now.ToString("ddMMyyHHmmss");
            // zero pad as needed
            mobileId = mobileId.PadLeft(4, '0');

            var importantContent = "6IDc#" + mobileId + (char)002 + "0" + "\r" + "0" +
                                "\r" + "1" + "\r" + "111000" + "\r" + "000" +
                            "\r" + "000" + "\r" +
                            "s" + chikDate + "" + (char)29 + "" + "00000000" + "" + (char)31 + "" + "00" + (char)30 + "" +
                            "01" + "\r\r" + mobileId + "\r" + "\r" + (char)003;

            var sql = "insert into cme( " +
                     "LogCodeID, LogCategoryID, MobileID, JobNumber, SubJobNumber, StatusID, Comments, ParcelTotal" +
                     ", ParcelSequence, Channel, Details, Retry, Tried, Modified, UserCode" +
                     ") VALUES(" +
                     "  0" + // LogCodeID - IN int(11)
                     ", 6 " + // LogCategoryID - IN int(11)
                     "," + mobileId + // MobileID - IN int(11)
                     ", ''" + // JobNumber - IN char(16)"
                     ", ''" + // SubJobNumber - IN char(2)
                     ", 02" + // StatusID - IN int(11)
                     "," + "''" +                    // Comments - IN mediumtext
                               "  , 0" + // ParcelTotal - IN int(11)
                               "  , 0" + // ParcelSequence - IN int(11)
                               "  , " + stateId + // Channel - IN int(11)
                               ", '" + importantContent + "'" +// Details - IN mediumtext
                               " , 0 " +  // Retry - IN int(11)
                               " , ''" +// Tried - IN char(25)
                               " , " + DateTime.Now.ToOADate() + // Modified - IN double
                               " , 0" +// UserCode - IN int(11)
                                    ")";

            return sql;

        }

        public static string GenerateFakeLogoutSql(string stateId, string mobileId)
        {
            byte chrGS = 0x29; // Start of Text
            byte chrRS = 0x31; // End of Text   
            var chikDate = DateTime.Now.ToString("ddMMyyHHmmss");
            // zero pad as needed
            mobileId = mobileId.PadLeft(4, '0');

            var importantContent = "6IDc#" + mobileId + (char)002 + "0" + "\r" + "0" +
                                "\r" + "1" + "\r" + "111000" + "\r" + "000" +
                            "\r" + "000" + "\r" +
                            "s" + chikDate + "" + (char)29 + "" + "00000000" + "" + (char)31 + "00" + (char)30 + "" +
                            "03" + "\r" + mobileId + "\r" +
                            "Shift End" + "\r\r" + (char)003;

            var sql = "insert into cme( " +
                     "LogCodeID, LogCategoryID, MobileID, JobNumber, SubJobNumber, StatusID, Comments, ParcelTotal" +
                     ", ParcelSequence, Channel, Details, Retry, Tried, Modified, UserCode" +
                     ") VALUES(" +
                     "  0" + // LogCodeID - IN int(11)
                     ", 6 " + // LogCategoryID - IN int(11)
                     "," + mobileId + // MobileID - IN int(11)
                     ", ''" + // JobNumber - IN char(16)"
                     ", ''" + // SubJobNumber - IN char(2)
                     ", 02" + // StatusID - IN int(11)
                     "," + "''" +                    // Comments - IN mediumtext
                               "  , 0" + // ParcelTotal - IN int(11)
                               "  , 0" + // ParcelSequence - IN int(11)
                               "  , " + stateId + // Channel - IN int(11)
                               ", '" + importantContent + "'" +// Details - IN mediumtext
                               " , 0 " +  // Retry - IN int(11)
                               " , ''" +// Tried - IN char(25)
                               " , " + DateTime.Now.ToOADate() + // Modified - IN double
                               " , 0" +// UserCode - IN int(11)
                                    ")";

            return sql;

        }
    }


    public struct DaylightSavings
    {
        public string DriverId;
        public string StateCode;
        public string StateZone;
    }
}