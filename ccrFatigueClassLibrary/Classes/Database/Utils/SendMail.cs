﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
//using configSettings = ILogixClassLibrary.Properties.Settings;
//using iLogixImage.Classes;
using ccrJobStatusUpdater.Classes.db.utils;

//using fatigueSettings = iCCRFatigueClassLibrary.Properties.Settings;

namespace iLogixImage.Classes
{
    public static class SendMail
    {
        static int _iRemoteErrors = 0;

        public static void SendBasicEmail(string Message, string type, ConfigStructures cfg, string state = "ITOnly")
        {
            string to = string.Empty;
            string cc = string.Empty;

           
            bool bOkToEmail = false;

            const string From = "donotreply@capitaltransport.com.au";
            to = "Software-Development@capitaltransport.com.au";
            // new code
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(From, "Fake Driver Login/Logout Engine");
            msg.To.Add(to);
            if (cc != string.Empty)
                msg.CC.Add(cc);
            msg.Subject = "iLogix Driver Fatigue " + (type == "INFO" ? "Information" : type);
            msg.IsBodyHtml = true;
            msg.Body = @Message;

 
            var client = new SmtpClient("isis.challengelogistics.com.au") { UseDefaultCredentials = true };
            // Credentials are necessary if the server requires the client  
            // to authenticate before it will send e-mail on the client's behalf.

            switch (type)
            {
                case "RemoteData":
                    // count the number of remote data issues and only send message when the 
                    // number exceeds a set value and reset counter. (** this is specifically for tPlus servers)
                    _iRemoteErrors++;
                    if (_iRemoteErrors > 100)
                    {
                        msg.Subject += string.Format("\n\nDuplicate Errors {0}", _iRemoteErrors);
                        bOkToEmail = true;
                        _iRemoteErrors = 0;
                    }
                    else if (_iRemoteErrors == 1)
                    {
                        bOkToEmail = true;
                    }
                    break;
                case "ITOnly":
                    // may use this for other messagees too?
                    bOkToEmail = true;
                    break;
                default:
                    _iRemoteErrors = 0;
                    bOkToEmail = true;
                    break;
            }

            try
            {
                if (bOkToEmail)
                    client.Send(msg);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception {0}",
                    ex.ToString());
            }
        }
    }
}