﻿using System;
using System.Configuration;
using System.Linq;

namespace ccrJobStatusUpdater.Classes.db.utils
{
    public struct ConfigStructures
    {
        public string MySQLConnection;
        public string DriverIdMelbourne;
        public string DriverIdSydney;
        public string DriverIdBrisbane;
        public string DriverIdAdelaide;
        public string DriverIdPerth;

        public string StateCodeMelbourne;
        public string StateCodeSydney;
        public string StateCodeBrisbane;
        public string StateCodeAdelaide;
        public string StateCodePerth;

        public bool ActivateVIC;
        public bool ActivateNSW;
        public bool ActivateQLD;
        public bool ActivateSA;
        public bool ActivateWA;

        public string StateZoneMelbourne;
        public string StateZoneSydney;
        public string StateZoneBrisbane;
        public string StateZoneAdelaide;
        public string  StateZonePerth;
        
        public string FirstTriggerTimeHour;
        public string SecondTriggerTimeHour;
    }

    /// <summary>
    /// This class provides database settings for CCR and TPlus db server for every state
    /// The settings are read from the App.config
    /// </summary>
    public class DatabaseSettings
    {
        //public static String MySQLDatabaseConnectionString { get; set; }
        //public DatabaseSettings()
        //{
        //    //read the settings from App.config
        //    MySQLDatabaseConnectionString = ConfigurationManager.AppSettings["MySQLConnection"];
        //}
        // http://stackoverflow.com/questions/3390750/how-to-use-int-tryparse-with-nullable-int
        // I have added another conversion here 
        // G. Anderson November 2013
        public static DateTime? NullableDate(string val)
        {
            DateTime outValue;
            return DateTime.TryParse(val, out outValue) ? (DateTime?)outValue : null;
        }

        public static int? NullableInt(string val)
        {
            int outValue;
            return int.TryParse(val, out outValue) ? (int?)outValue : null;
        }
    }
}