﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CCRJobStatus.Classes.Database.Utils
{
    // uses following pattern for raising events rather than one handler
    // for all events as per the original solutions from Sigtec
    // http://www.c-sharpcorner.com/UploadFile/rmcochran/customgenericeventargs05132007100456AM/customgenericeventargs.aspx
    // ***************************************************
    //
    //          *** WARNING ***
    // This code is used extensively in the application
    // Please do not edit except on a separate dev path
    //          *** WARNING ***
    //
    // ***************************************************
    // http://devlicious.com/blogs/christopher_bennage/archive/2007/09/13/my-new-little-friend-enum-lt-t-gt.aspx
    public static class Enum<T>
    {
        // call like so: MyEnum enumValue = Enum<MyEnum>.Parse(stringValue);  
        public static T Parse(string value)
        {
            return (T)Enum.Parse(typeof(T), value);
        }

        public static IList<T> GetValues()
        {
            IList<T> list = new List<T>();

            foreach (object value in Enum.GetValues(typeof(T)))
            {
                list.Add((T)value);
            }
            return list;
        }
    }

    public class EventArgs<T> : EventArgs
    {
        public EventArgs(T value)
        {
            m_value = value;
        }

        private T m_value;

        public T Value
        {
            get
            {
                return m_value;
            }
        }
    }

    public class EventArgs<T, U> : EventArgs<T>
    {
        public EventArgs(T value, U value2) : base(value)
        {
            m_value2 = value2;
        }

        private U m_value2;

        public U Value2
        {
            get
            {
                return m_value2;
            }
        }
    }

    public class EventArgs<T, U, V> : EventArgs<T, U>
    {
        public EventArgs(T value, U value2, V value3) : base(value, value2)
        {
            m_value3 = value3;
        }

        private V m_value3;

        public V Value3
        {
            get
            {
                return m_value3;
            }
        }
    }

    //public static class Enum<T>
    //{
    //    public static T Parse(string value)
    //    {
    //        return (T)Enum.Parse(typeof(T), value);
    //    }

    //    public static IList<T> GetValues()
    //    {
    //        IList<T> list = new List<T>();
    //        foreach (object value in Enum.GetValues(typeof(T)))
    //        {
    //            list.Add((T)value);
    //        }
    //        return list;
    //    }
    //}
}