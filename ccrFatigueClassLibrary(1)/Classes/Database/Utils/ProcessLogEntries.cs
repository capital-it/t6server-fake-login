﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Collections;
using MySql.Data.MySqlClient;
using ccrJobStatusUpdater.Classes.db.utils;
using ccrJobStatusUpdater.Classes;
using System.Diagnostics;
using System.Data;
using fatigueSettings = iCCRFatigueClassLibrary.Properties.Settings;

namespace ILogixClassLibrary.Classes 
{
    public class ProcessLogEntries
    {
        bool running = false;
        private readonly dbServiceLayer _msg = Singleton<dbServiceLayer>.UniqueInstance;
        private DateTime _lastRun = DateTime.Now;
        int _iDrivers = 0;
        int hoursToFlagBreach = fatigueSettings.Default.HoursWorkedTimeBreach;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public ProcessLogEntries()
        {
            running = true;
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            running = false;
        }

        /// <summary>
        /// Called by the <see cref="T:Quartz.IScheduler" /> when a <see cref="T:Quartz.ITrigger" />
        /// fires that is associated with the <see cref="T:Quartz.IJob" />.
        /// </summary>
        /// <param name="context">The execution context.</param>
        /// <remarks>
        /// The implementation may wish to set a  result object on the
        /// JobExecutionContext before this method exits.  The result itself
        /// is meaningless to Quartz, but may be informative to
        /// <see cref="T:Quartz.IJobListener" />s or
        /// <see cref="T:Quartz.ITriggerListener" />s that are watching the job's
        /// execution.
        /// </remarks>
        public void Create(Quartz.IJobExecutionContext context)
        {
            #region setup all the states which can be enabled
            
            // setup our new daylight savings struct
            var itm = new DaylightSavings();
            
            var rangeTimeZonesList = new List<DaylightSavings>();
            // 2 checks here, first is the state active and second, is the trigger activated
            // for the state itself? 
            if (fatigueSettings.Default.ActivateVIC & context.Trigger.Key.ToString().Contains("VIC"))
            {
                itm.DriverRange = "1 AND 1999";
                itm.StateId = "VIC";
                rangeTimeZonesList.Add(itm);
            }
            if (fatigueSettings.Default.ActivateNSW & context.Trigger.Key.ToString().Contains("NSW"))
            {
                itm.DriverRange = "2000 AND 3999";
                itm.StateId = "NSW";
                rangeTimeZonesList.Add(itm);
            }
            if (fatigueSettings.Default.ActivateQLD & context.Trigger.Key.ToString().Contains("QLD"))
            {
                itm.DriverRange = "4000 AND 5999";
                itm.StateId = "QLD";
                rangeTimeZonesList.Add(itm);
            }
            if (fatigueSettings.Default.ActivateSA & context.Trigger.Key.ToString().Contains("SA"))
            {
                itm.DriverRange = "8000 AND 9999";
                itm.StateId = "SA";
                rangeTimeZonesList.Add(itm);
            }
            if (fatigueSettings.Default.ActivateWA & context.Trigger.Key.ToString().Contains("WA"))
            {
                itm.DriverRange = "6000 AND 7999";
                itm.StateId = "WA";
                rangeTimeZonesList.Add(itm);
            }
            
            
            #endregion
            
            using (var mySql = new MySqlConnection(fatigueSettings.Default.MySQLConnection))
            {
                while (running)
                {
                    mySql.Open();
                    // *****************
                    // steps to follow
                    // ***************** 
                    // find out the images folders for each state
                    foreach (var state in rangeTimeZonesList) 
                    {
                        var htmlbody = string.Empty;
                        //var ds = new DataSet();
                        // get a header ready to insert data
                        string htmlHeader = _msg.HtmlReportHeader();
                        int mixUp = 0;
                        // work on one state result set - returned in a DataSet object for churning
                        var ds = _msg.CollectDriverTimesForRange(state.DriverRange, mySql);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            _iDrivers = 0;
                            do
                            {
                                // grab driver and time seen
                                DataRow dr = ds.Tables[0].Rows[_iDrivers];
                                string driverId = dr["MobileID"].ToString().PadLeft(4, '0');
                                DateTime dateTimeFirstSeen = DateTime.FromOADate(Convert.ToDouble(dr["Modified"].ToString()));
                                // get driver and last modified date
                                DateTime dateTimeLastSeen = DateTime.FromOADate(_msg.CollectSingleDriverLastTimeSeen(driverId, mySql));
                                // find difference using timespan
                                TimeSpan timeHrs = dateTimeLastSeen - dateTimeFirstSeen;
                                // do some date/time arithmetic and see if a breach has occurred
                                if (timeHrs.Hours > hoursToFlagBreach)
                                {
                                    // get a count of logins for today
                                    int totalLogins = _msg.CountTimesLoggedInToday(driverId, mySql);
                                    // add to the html string 
                                    htmlbody += "<tr>" +
                                                "<td class='" + (mixUp == 0 ? "tg-vn4c" : "tg-031e") + "'>" + driverId + "</td>" +
                                                "<td class='" + (mixUp == 0 ? "tg-vn4c" : "tg-031e") + "'>" + dateTimeFirstSeen.ToString("hh:mm:ss tt") + "</td>" +
                                                "<td class='" + (mixUp == 0 ? "tg-vn4c" : "tg-031e") + "'>" + dateTimeLastSeen.ToString("hh:mm:ss tt") + "</td>" +
                                                "<td class='" + (mixUp == 0 ? "tg-vn4c" : "tg-031e") + "'>" + timeHrs.Hours + "</td>" +
                                                "<td class='" + (mixUp == 0 ? "tg-vn4c" : "tg-031e") + "'>" + totalLogins + "</td>" +
                                                "</tr>";
                                    mixUp = mixUp == 0 ? 1 : 0;
                                }
                                // increment driver count
                                _iDrivers++;
                            }
                            while (_iDrivers < ds.Tables[0].Rows.Count);
                            // create report for this state if rows affected
                            if (htmlbody.Length > 0)
                            {
                                const string htmlCloseTable = "</table>" +
                                                              "<div class='more-info'>More info: <a href='http://my.capitaltransport.com.au/IT/_layouts/15/start.aspx#/'>Capital WiKi (Login required)</a></div>" +
                                                              "</body></html>";
                                // should give valid html for the emails
                                htmlHeader += htmlbody + htmlCloseTable;
                                
                                // clean up the files 
                                iLogixImage.Classes.SendMail.SendBasicEmail(string.Format("Details of drivers who may have worked in excess of the award.{0}{1}Please note that the results shown should NOT be " +
                                    "relied upon soley to identify drivers in breach, it should be used " +
                                    "along with other resources available.", Environment.NewLine, htmlHeader), "INFO", state.StateId);
                            }
                        }
                    }
                    mySql.Close();   
                    running = false;
                }
            }
        }
    }
    
    public struct DaylightSavings
    {
        public string DriverRange;
        public string StateId;
    }
}