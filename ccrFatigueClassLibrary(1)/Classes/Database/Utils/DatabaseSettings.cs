﻿using System;
using System.Configuration;
using System.Linq;

namespace ccrJobStatusUpdater.Classes.db.utils
{
    /// <summary>
    /// This class provides database settings for CCR and TPlus db server for every state
    /// The settings are read from the App.config
    /// </summary>
    public class DatabaseSettings
    {
        public static String MySQLDatabaseConnectionString { get; set; }

        public DatabaseSettings()
        {
            //read the settings from App.config
            MySQLDatabaseConnectionString = ConfigurationManager.AppSettings["MySQLConnection"];
        }

        // http://stackoverflow.com/questions/3390750/how-to-use-int-tryparse-with-nullable-int
        // I have added another conversion here 
        // G. Anderson November 2013
        public static DateTime? NullableDate(string val)
        {
            DateTime outValue;
            return DateTime.TryParse(val, out outValue) ? (DateTime?)outValue : null;
        }

        public static int? NullableInt(string val)
        {
            int outValue;
            return int.TryParse(val, out outValue) ? (int?)outValue : null;
        }
    }
}