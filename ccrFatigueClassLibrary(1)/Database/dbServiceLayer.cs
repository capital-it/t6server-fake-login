﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;
using ccrJobStatusUpdater.Classes.db.utils;
using ILogixClassLibrary.Classes;
using fatigueSettings = iCCRFatigueClassLibrary.Properties.Settings;

namespace ILogixClassLibrary
{
    #region Singleton
    
    // private readonly dbServiceLayer _msg = Singleton<dbServiceLayer>.UniqueInstance;
    // messages that will be sent to DB Server are handled here
    // dbServiceLayer DbMessageHandler = Singleton<dbServiceLayer>.UniqueInstance;
    public class Singleton<T> where T : class, new()
    {
        Singleton()
        {
        }
        
        class SingletonCreator
        {
            static SingletonCreator()
            {
            }
            
            // Private object instantiated with private constructor
            internal static readonly T instance = new T();
        }
        
        public static T UniqueInstance
        {
            get
            {
                return SingletonCreator.instance;
            }
        }
    }
    
    #endregion 
    
    class dbServiceLayer
    {
        #region Collect header for html report 
        
        /// <summary>
        /// Will return a header for reporting of data returned. Insert data in each row
        /// and finish off with the </table> command 
        ///  <tr>
        //          <td class="tg-vn4c"></td>
        //          <td class="tg-vn4c"></td>
        //          <td class="tg-vn4c"></td>
        //          <td class="tg-vn4c"></td>
        //    </tr>
        //      <tr>
        //          <td class="tg-031e">4</td>
        //          <td class="tg-031e">5</td>
        //          <td class="tg-031e">7</td>
        //          <td class="tg-031e">8</td>
        //      </tr>
        //  </table>
        /// 
        /// </summary>
        /// <returns></returns>
        internal string HtmlReportHeader()
        {
            string htmlhead = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>" +
                          "<html>" +
                          "<head>" +
                          "<title>Capital Transport Fatigue Management</title>" +
                          "</head>" +
                          "<body>" +
                         string.Format("<p>Summary of drivers who MAY have worked {0} or more hours today.</p>", fatigueSettings.Default.HoursWorkedTimeBreach);
         

            string html = "<style type='text/css'>" +
                          ".tg  {border-collapse:collapse;border-spacing:0;border-color:#999;}" +
                          ".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}" +
                          ".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}" +
                          ".tg .tg-vn4c{background-color:#D2E4FC}" +
                          "</style>";
                    
            html += "<table class='tg'>" +
                    "<tr>" +
                    "<th class='tg-031e'>Driver <br></th>" +
                    "<th class='tg-031e'>Login Time<br></th>" +
                    "<th class='tg-031e'>Last Time Seen<br></th>" +
                    "<th class='tg-031e'># Hours</th>" +
                    "<th class='tg-031e'># Logins</th>" +
                    "</tr>";
        
            return htmlhead + html;
        }

        #endregion

        #region Collect items for the passed in range of drivers
        
        internal DataSet CollectDriverTimesForRange(string driverRange, MySqlConnection mySqlConn)
        {
            DataSet ds = new DataSet();
            string sql = string.Empty;
            double dateFrom = Math.Truncate(DateTime.Now.ToOADate());
            
            // get the rows for the current range of drivers
            sql = "SELECT MobileID, Modified FROM log WHERE StatusID ='4' and Modified > " + dateFrom + " " +
                  "AND MobileId BETWEEN " + driverRange + " GROUP BY MobileID";
            // open the connection to  rows
            MySqlDataAdapter myda = new MySqlDataAdapter(sql, mySqlConn);
            // fill the dataset
            myda.Fill(ds, "MySQLData");
            // could use a datareader but might need to re-read rows later?
            return ds;
        }

        #endregion
        
        #region Get values for one mobile id
        
        internal double CollectSingleDriverLastTimeSeen(string mobileId, MySqlConnection mySqlConn)
        {
            double dateFound = -1;
            string sql = string.Empty;
            
            // gets last modified records for a mobile
            sql = "SELECT Modified FROM LOG " +
                  "WHERE MobileID = '" + mobileId + "' " +
                  "ORDER BY modified DESC " +
                  "LIMIT 1";
            
            MySqlCommand cmd = new MySqlCommand(sql, mySqlConn);
            // executeScalar will return one value
            dateFound = Convert.ToDouble(cmd.ExecuteScalar() + "");
            
            // convert the value to double 
            return dateFound;
        }

        #endregion 

        #region Check number of times logged in today
        
        internal int CountTimesLoggedInToday(string mobileId, MySqlConnection mySqlConn)
        {
            int rowCount = -1;
            double dateFrom = Math.Truncate(DateTime.Now.ToOADate());
            string sql = string.Empty;
            
            // gets last modified records for a mobile
            sql = "SELECT COUNT(*) FROM " +
                  "log " +
                  "WHERE StatusID ='4' AND MobileID = '" +
                  mobileId + "' AND Modified > " + dateFrom;
            
            MySqlCommand cmd = new MySqlCommand(sql, mySqlConn);
            // executeScalar will return one value
            rowCount = Convert.ToInt16(cmd.ExecuteScalar() + "");
            
            // convert the value to double 
            return rowCount;
        }

        #endregion 
    }
}