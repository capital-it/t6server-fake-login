﻿using System;
using System.Linq;
using ILogixClassLibrary.Classes;

namespace ILogixClassLibrary
{
    public class clsProcessor
    {
        ProcessLogEntries logParser;

        public void StartProcessing(Quartz.IJobExecutionContext context)
        {
            logParser = new ProcessLogEntries();
            logParser.Create(context);
        }

        public void StopProcessing()
        {
            
        }
    }
}