﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.ServiceProcess;
using System.Windows.Forms;
using ccrJobStatusUpdater.Classes.db.utils;
using ILogixClassLibrary;
using ILogixClassLibrary.Classes;
using iLogixImage.Classes;
using iLogixProcessor;
using NodaTime;
using Quartz;
using Quartz.Impl;
//using iSystemSettings = iLogixProcessor.Properties.Settings; // iCCRFatigue.Properties.Settings;

namespace WinformTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private IScheduler _scheduler = StdSchedulerFactory.GetDefaultScheduler();
        private ConfigStructures cfg;


        private void Form1_Load(object sender, EventArgs e)
        {
            var appSettings = ConfigurationManager.AppSettings;

            cfg = new ConfigStructures
            {
                ActivateVIC = Convert.ToBoolean(appSettings["ActivateVIC"]),
                ActivateNSW = Convert.ToBoolean(appSettings["ActivateNSW"]),
                ActivateQLD = Convert.ToBoolean(appSettings["ActivateQLD"]),
                ActivateSA = Convert.ToBoolean(appSettings["ActivateSA"]),
                ActivateWA = Convert.ToBoolean(appSettings["ActivateWA"]),

                MySQLConnection = appSettings["MySQLConnection"],

                DriverIdMelbourne = appSettings["DriverIdMelbourne"],
                DriverIdSydney = appSettings["DriverIdSydney"],
                DriverIdBrisbane = appSettings["DriverIdBrisbane"],
                DriverIdAdelaide = appSettings["DriverIdAdelaide"],
                DriverIdPerth = appSettings["DriverIdPerth"],

                StateCodeMelbourne = appSettings["StateCodeMelbourne"],
                StateCodeSydney = appSettings["StateCodeSydney"],
                StateCodeBrisbane = appSettings["StateCodeBrisbane"],
                StateCodeAdelaide = appSettings["StateCodeAdelaide"],
                StateCodePerth = appSettings["StateCodePerth"],


                StateZoneMelbourne = appSettings["StateZoneMelbourne"],
                StateZoneSydney = appSettings["StateZoneSydney"],
                StateZoneBrisbane = appSettings["StateZoneBrisbane"],
                StateZoneAdelaide = appSettings["StateZoneAdelaide"],
                StateZonePerth = appSettings["StateZonePerth"],


                FirstTriggerTimeHour = appSettings["FirstTriggerTimeHour"],
                SecondTriggerTimeHour = appSettings["SecondTriggerTimeHour"]
            };


            #region Timezone setup settings and setup here

           

            // and start it off
            _scheduler.Start();
           
             //  SendMail.SendBasicEmail( string.Format("Trigger Hour {0}, Trigger 2nd Hour {1}", cfg.FirstTriggerTimeHour,cfg.SecondTriggerTimeHour) , "ITOnly", cfg);

            //SendMail.SendBasicEmail(String.Format("CCR Fatigue Scheduler Process engine has started and triggers will commence @ {0} and {1}",
            //    cfg.FirstTriggerTimeHour, cfg.SecondTriggerTimeHour) + "<br><br>" +
            //                        "This is an information message only, no action is required.", "ITOnly", cfg);

            // SendMail.SendBasicEmail(string.Format("Quartz Trigger Hour {0}, Quartz Trigger 2nd Hour{1}", quartzFirstCheckTime, quartzSecondCheckTime), "ITOnly", cfg);
            //SendMail.SendBasicEmail(
            //    String.Format(
            //        "CCR Fatigue Scheduler Process engine has started and triggers will commence @ {0} and {1} AEST and will be adjusted for daylight savings in other states.",
            //        cfg.FirstTriggerTimeHour, cfg.SecondTriggerTimeHour) + "<br><br>" +
            //    "This is an information message only, no action is required.", "ITOnly", cfg);


            #endregion  End TimeZone settings and setup here

            try
            {

                #region Job Scheduler creation

                //add new jobs on the scheduler            
                var xmlShredderJob = JobBuilder.Create<FatigueJobStartUp>()
                    .WithIdentity("XmlShredder", "dataextractiongroup")
                    .Build();
                //create a triger condition
                var trigger = TriggerBuilder.Create()
                    .WithIdentity("XmlShredder", "dataextractiongroup")
                    .WithDailyTimeIntervalSchedule(
                        x => x.StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(6, 0))
                            .EndingDailyAt(TimeOfDay.HourAndMinuteOfDay(23, 0))
                            .WithIntervalInMinutes(2))
                    .Build();
                //schedule these jobs
                _scheduler.ScheduleJob(xmlShredderJob, trigger);


 

                #endregion End Scheduler creation

                #region Melbourne - no need to frig around with timezone


                #endregion Melbourne

                #region Sydney - no need to frig around with timezone

                #endregion Sydney

                #region Brisbane - no need to frig around with timezone

                   #endregion Brisbane

                #region Adelaide - timezone will be adjusted by .30 minutes during daylight savings

   
                #endregion Adelaide

                #region Perth - timezone will be adjusted by 3 hours during EST daylight savings (and other times?)

   

                #endregion Perth

                #region  Tell quartz to schedule the job using our triggers

                //SendMail.SendBasicEmail("CCR Fatigue Scheduler Process engine has gone beserk 111. " +
                //                       "\r\n\r\nError Message: " + @"\r\n" +
                //                       "Please review as no automated reporting will be created.", "ITOnly", cfg);
                //try
                //{
                //scheduler.ScheduleJob(job1, triggerMel00I);
               
                //SendMail.SendBasicEmail("CCR Fatigue Scheduler Process engine has gone beserk 123. " +
                //       "\r\n\r\nError Message: " + @"\r\n" +
                //       "Please review as no automated reporting will be created.", "ITOnly", cfg);
            }
            catch (Exception ex)
            {
                Debug.Write((ex.Message));
                SendMail.SendBasicEmail("CCR Fatigue Scheduler Process engine has gone beserk. " +
                                        "<br><br>Error Message: " + ex.Message + "<br><br>" +
                                        "Please review as no automated reporting will be created.", "ITOnly", cfg);
                Application.Exit();
            }

            #endregion

        }

        protected void OnStop()
        {
            if (_scheduler != null)
                _scheduler.Shutdown();
        }

        private void cmdStart_Click(object sender, EventArgs e)
        {
          //  ProcessLogEntries rip = new ProcessLogEntries();
            
         //    FatigueJobStartUp fatup = new FatigueJobStartUp();
             
        }
    }

    public class FatigueJobStartUp : IJob
    {
        private ILogixClassLibrary.clsProcessor iCCRFatigue;
        private ConfigStructures cfg;

        public FatigueJobStartUp()
        {
            iCCRFatigue = new clsProcessor();
        }

        public void Execute(IJobExecutionContext context)
        {
            
            #region annoying feature, send in config settings so the class does not have to

            var appSettings = ConfigurationManager.AppSettings;

            cfg = new ConfigStructures
            {
                ActivateVIC = Convert.ToBoolean(appSettings["ActivateVIC"]),
                ActivateNSW = Convert.ToBoolean(appSettings["ActivateNSW"]),
                ActivateQLD = Convert.ToBoolean(appSettings["ActivateQLD"]),
                ActivateSA = Convert.ToBoolean(appSettings["ActivateSA"]),
                ActivateWA = Convert.ToBoolean(appSettings["ActivateWA"]),

                MySQLConnection = appSettings["MySQLConnection"],

                DriverIdMelbourne = appSettings["DriverIdMelbourne"],
                DriverIdSydney = appSettings["DriverIdSydney"],
                DriverIdBrisbane = appSettings["DriverIdBrisbane"],
                DriverIdAdelaide = appSettings["DriverIdAdelaide"],
                DriverIdPerth = appSettings["DriverIdPerth"],

                StateCodeMelbourne = appSettings["StateCodeMelbourne"],
                StateCodeSydney = appSettings["StateCodeSydney"],
                StateCodeBrisbane = appSettings["StateCodeBrisbane"],
                StateCodeAdelaide = appSettings["StateCodeAdelaide"],
                StateCodePerth = appSettings["StateCodePerth"],

                StateZoneMelbourne = appSettings["StateZoneMelbourne"],
                StateZoneSydney = appSettings["StateZoneSydney"],
                StateZoneBrisbane = appSettings["StateZoneBrisbane"],
                StateZoneAdelaide = appSettings["StateZoneAdelaide"],
                StateZonePerth = appSettings["StateZonePerth"],

                FirstTriggerTimeHour = appSettings["FirstTriggerTimeHour"],
                SecondTriggerTimeHour = appSettings["SecondTriggerTimeHour"]
            };

            #endregion

            iCCRFatigue.StartProcessing(context, cfg);
        }

        protected void OnStop()
        {
            SendMail.SendBasicEmail("Fatigue Management Process engine is *** STOPPED *** " +
                                    "\r\n\r\n" +
                                    "This is an information message only, please refer to IT Support documentation \r\n" +
                                    @"which is located IT Support Folder", "ITOnly", cfg);

            iCCRFatigue.StopProcessing();
            Application.Exit();
        }

       
    }

}

 