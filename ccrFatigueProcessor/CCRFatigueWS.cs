﻿using System;
using System.Diagnostics;
using System.Configuration;
using System.ServiceProcess;
using ccrJobStatusUpdater.Classes.db.utils;
using ILogixClassLibrary;
using Quartz;
using Quartz.Impl;

//using iSystemSettings = iLogixProcessor.Properties.Settings; // iCCRFatigue.Properties.Settings;// iCCRFatigueClassLibrary.Properties.Settings; iCCRFatigue.Properties
using iLogixImage.Classes;


namespace iLogixProcessor
{
    public partial class CCRFatigueWS : ServiceBase
    {
        // Grab the Scheduler instance from the Factory 
        protected IScheduler _scheduler = StdSchedulerFactory.GetDefaultScheduler();
        private ConfigStructures cfg;

        public CCRFatigueWS()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            #region annoying feature, send in config settings so the class does not have to

            var appSettings = ConfigurationManager.AppSettings;

            cfg = new ConfigStructures
            {
                MySQLConnection = appSettings["MySQLConnection"],
                FirstTriggerTimeHour = appSettings["FirstTriggerTimeHour"],
                SecondTriggerTimeHour = appSettings["SecondTriggerTimeHour"]
            };

            #endregion

            StartUp();
        }

        protected void StartUp()
        {
            #region Timezone setup settings and setup here

            // and start it off
            _scheduler.Start();

            // Trigger the job to run @ XXpm times
            int quartzFirstCheckTime = Convert.ToInt16(cfg.FirstTriggerTimeHour);
            int quartzSecondCheckTime = Convert.ToInt16(cfg.SecondTriggerTimeHour);

            SendMail.SendBasicEmail(
                String.Format(
                    "PDA Fake Login/Logout engine has started and triggers will commence at {0}:00 and stop at {1}:00.",
                    quartzFirstCheckTime, quartzSecondCheckTime) + "<br><br>" +
                "This is an information message only, no action is required.", "Contact IT Team", cfg);

            #endregion  End TimeZone settings and setup here

            try
            {
                #region Job Scheduler creation

                //add new jobs on the scheduler            
                var xmlShredderJob = JobBuilder.Create<FatigueJobStartUp>()
                    .WithIdentity("XmlShredder", "dataextractiongroup")
                    .Build();
                //create a triger condition
                var trigger = TriggerBuilder.Create()
                    .WithIdentity("XmlShredder", "dataextractiongroup")
                    .WithDailyTimeIntervalSchedule(
                        x => x.StartingDailyAt(TimeOfDay.HourAndMinuteOfDay( quartzFirstCheckTime , 0))
                            .EndingDailyAt(TimeOfDay.HourAndMinuteOfDay( quartzSecondCheckTime, 0))
                            .WithIntervalInMinutes(2))
                    .Build();
                //schedule these jobs
                _scheduler.ScheduleJob(xmlShredderJob, trigger);




                #endregion End Scheduler creation

            }
            catch (Exception ex)
            {
                Debug.Write((ex.Message));
                SendMail.SendBasicEmail("Device Fake Login Process engine has gone beserk. " +
                                        "<br><br>Error Message: " + ex.Message + "<br><br>" +
                                        "Please review as no automated reporting will be created.", "ITOnly", cfg);
            }
        }

        protected override void OnStop()
        {
            if (_scheduler != null)
                _scheduler.Shutdown();
        }
    }

    [DisallowConcurrentExecution]
    public class FatigueJobStartUp : IJob
    {
        //ILogixClassLibrary.clsProcessor iCCRFatigue;
        private clsProcessor iDriverMunting; 
        private ConfigStructures cfg;
        
        public void Execute(IJobExecutionContext context)
        {
            iDriverMunting = new clsProcessor();
        
            #region annoying feature, send in config settings so the class does not have to

            var appSettings = ConfigurationManager.AppSettings;

            cfg = new ConfigStructures
            {
                ActivateVIC = Convert.ToBoolean(appSettings["ActivateVIC"]),
                ActivateNSW = Convert.ToBoolean(appSettings["ActivateNSW"]),
                ActivateQLD = Convert.ToBoolean(appSettings["ActivateQLD"]),
                ActivateSA = Convert.ToBoolean(appSettings["ActivateSA"]),
                ActivateWA = Convert.ToBoolean(appSettings["ActivateWA"]),

                MySQLConnection = appSettings["MySQLConnection"],

                DriverIdMelbourne = appSettings["DriverIdMelbourne"],
                DriverIdSydney = appSettings["DriverIdSydney"],
                DriverIdBrisbane = appSettings["DriverIdBrisbane"],
                DriverIdAdelaide = appSettings["DriverIdAdelaide"],
                DriverIdPerth = appSettings["DriverIdPerth"],

                StateCodeMelbourne = appSettings["StateCodeMelbourne"],
                StateCodeSydney = appSettings["StateCodeSydney"],
                StateCodeBrisbane = appSettings["StateCodeBrisbane"],
                StateCodeAdelaide = appSettings["StateCodeAdelaide"],
                StateCodePerth = appSettings["StateCodePerth"],

                StateZoneMelbourne = appSettings["StateZoneMelbourne"],
                StateZoneSydney = appSettings["StateZoneSydney"],
                StateZoneBrisbane = appSettings["StateZoneBrisbane"],
                StateZoneAdelaide = appSettings["StateZoneAdelaide"],
                StateZonePerth = appSettings["StateZonePerth"],

                FirstTriggerTimeHour = appSettings["FirstTriggerTimeHour"],
                SecondTriggerTimeHour = appSettings["SecondTriggerTimeHour"]
            };

            #endregion

            iDriverMunting.StartProcessing(context, cfg);

//            SendMail.SendBasicEmail("Completed Running the Execute Method", "ITOnly", cfg);
        }

        public void OnStop()
        {
            iDriverMunting.StopProcessing();
        }
    }
}